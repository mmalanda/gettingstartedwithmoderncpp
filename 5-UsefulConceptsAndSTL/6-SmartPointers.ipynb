{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# [Getting started in C++](./) - [Useful concepts and STL](./0-main.ipynb) - [Smart pointers](./6-SmartPointers.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc": true
   },
   "source": [
    "<h1>Table of contents<span class=\"tocSkip\"></span></h1>\n",
    "<div class=\"toc\"><ul class=\"toc-item\"><li><span><a href=\"#Introduction\" data-toc-modified-id=\"Introduction-1\">Introduction</a></span></li><li><span><a href=\"#unique_ptr\" data-toc-modified-id=\"unique_ptr-2\"><code>unique_ptr</code></a></span><ul class=\"toc-item\"><li><span><a href=\"#Usage-to-store-data-in-a-class\" data-toc-modified-id=\"Usage-to-store-data-in-a-class-2.1\">Usage to store data in a class</a></span></li><li><span><a href=\"#Releasing-a-unique_ptr\" data-toc-modified-id=\"Releasing-a-unique_ptr-2.2\">Releasing a <code>unique_ptr</code></a></span></li></ul></li><li><span><a href=\"#shared_ptr\" data-toc-modified-id=\"shared_ptr-3\"><code>shared_ptr</code></a></span></li><li><span><a href=\"#Efficient-storage-with-vectors-of-smart-pointers\" data-toc-modified-id=\"Efficient-storage-with-vectors-of-smart-pointers-4\">Efficient storage with vectors of smart pointers</a></span><ul class=\"toc-item\"><li><ul class=\"toc-item\"><li><span><a href=\"#Using-a-trait-as-syntactic-sugar\" data-toc-modified-id=\"Using-a-trait-as-syntactic-sugar-4.0.1\">Using a trait as syntactic sugar</a></span></li></ul></li></ul></li></ul></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In short, **smart pointers** are the application of [RAII](./2-RAII.ipynb) to pointers: objects which handle more nicely the acquisition and release of dynamic allocation.\n",
    "\n",
    "There are many ways to define the behaviour of a smart pointer (the dedicated chapter in \\cite{Alexandrescu2001} is a very interesting read for this, especially as it uses heavily the template [policies](../4-Templates/5-MoreAdvanced.ipynb#Policies) to implement his):\n",
    "\n",
    "* How the pointer might be copied (or not).\n",
    "* When is the memory freed.\n",
    "* Whether `if (ptr)` syntax is accepted\n",
    "* ...\n",
    "\n",
    "The STL made the choice of providing two (and a half in fact...) kinds of smart pointers (introduced in C++ 11):\n",
    "\n",
    "* **unique pointers**\n",
    "* **shared pointers** (and the **weak** ones that goes along with them).\n",
    "\n",
    "One should also mention for legacy the first attempt: **auto pointers**, which were removed in C++ 17: you might encounter them in some libraries, but by all means don't use them yourself (look for *sink effect* on the Web if you want to know why).\n",
    "\n",
    "By design all smart pointers keep the whole syntax semantic:\n",
    "* `*` to dereference the (now smart) pointer.\n",
    "* `->` to access an attribute of the underlying object.\n",
    "\n",
    "Smart pointers are clearly a very good way to handle the ownership of a given object. \n",
    "\n",
    "This does not mean they supersede entirely ordinary (often called **raw** or more infrequently **dumb**) pointers: raw pointers might be a good choice to pass an object as a function parameter (see the discussion for the third question in this [Herb Sutter's post blog](https://herbsutter.com/2013/06/05/gotw-91-solution-smart-pointer-parameters/)). The raw pointer behind a smart pointer may be accessed through the `get()` method.\n",
    "\n",
    "Both smart pointers exposed below may be constructed directly from a raw pointer; in this case they take the responsibility of destroying the pointer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "#include <iostream>\n",
    "\n",
    "struct Foo\n",
    "{\n",
    "    ~Foo() \n",
    "    {\n",
    "        std::cout << \"Destroy foo\"<< std::endl;\n",
    "    }\n",
    "    \n",
    "};\n",
    "\n",
    "{\n",
    "    Foo* raw = new Foo;\n",
    "    \n",
    "    std::unique_ptr<Foo> unique(raw); // Now unique_ptr is responsible for pointer ownership: don't call delete\n",
    "                                      // on `raw`! Destructor of unique_ptr will call the `Foo` destructor.\n",
    "    \n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `unique_ptr`\n",
    "\n",
    "This should be your first choice for a smart pointer.\n",
    "\n",
    "The idea behind this smart pointer is that it can't be copied: there is exactly one instance of the smart pointer, and when this instance becomes out of scope the resources are properly released.\n",
    "\n",
    "In C++ 11 you had to use the classic `new` syntax to create one, but C++ 14 introduced a specific syntax `make_unique`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "\n",
    "{\n",
    "    auto ptr = std::make_unique<int>(5);\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The parenthesis takes the constructor arguments.\n",
    "\n",
    "The smart pointer can't be copied, but it can be moved:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "\n",
    "{\n",
    "    auto ptr = std::make_unique<int>(5);\n",
    "    auto copy = ptr; // COMPILATION ERROR: can't be copied!    \n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    auto ptr = std::make_unique<int>(5);\n",
    "    auto copy = std::move(ptr);\n",
    "    \n",
    "    // std::cout << \"Beware as now there are no guarantee upon the content of ptr: \" << *ptr << std::endl;\n",
    "    // < This line is invalid (using `ptr` after move is undefined behaviour) and makes Xeus-cling crash\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual with move semantics, beware in this second case: ptr is undefined after the `move` occurred... (this code run on [Coliru](http://coliru.stacked-crooked.com/a/a1aa87e64f64c9e8) leads to a more explicit segmentation fault).\n",
    "\n",
    "### Usage to store data in a class\n",
    "\n",
    "`std::unique_ptr` are a really good choice to store objects in a class, especially ones that do not have a default constructor. The underlying object may be accessed through reference or raw pointer; usually your class may look like:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <string>\n",
    "\n",
    "// Class which will be stored in another one through a `unique_ptr`\n",
    "class Content\n",
    "{\n",
    "    public:\n",
    "        \n",
    "        Content(std::string&& text); // notice: no default constructor!\n",
    "        \n",
    "        const std::string& GetValue() const;\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        std::string text_ = \"\";\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Content::Content(std::string&& text)\n",
    ": text_(text)\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "const std::string& Content::GetValue() const\n",
    "{\n",
    "    return text_;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <memory>\n",
    "\n",
    "class WithUniquePtr\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        WithUniquePtr(std::string&& text);\n",
    "\n",
    "        const Content& GetContent() const; // adding `noexcept` would be even better but Xeus-cling \n",
    "                                           // doesn't like it!\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        // A pointer of sort is required here:\n",
    "        // - No default constructor so `Content` can't be stored directly.\n",
    "        // - A reference would mean the object is effectively stored elsewhere; we assume \n",
    "        // we intend here to store the content in the current class.\n",
    "        std::unique_ptr<Content> content_ = nullptr;\n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "WithUniquePtr::WithUniquePtr(std::string&& text)\n",
    ": content_(std::make_unique<Content>(std::move(text)))\n",
    "{ }"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <cassert>\n",
    "\n",
    "const Content& WithUniquePtr::GetContent() const \n",
    "{\n",
    "    assert(content_ != nullptr);\n",
    "    return *content_;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doing so:\n",
    "\n",
    "* `Content` is stored by a `unique_ptr`, which will manage the destruction in due time of the object (when the `WithUniquePtr` object will be destroyed).\n",
    "* `Content` object might be manipulated through its reference; end-user don't even need to know resource was stored through a (smart) pointer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "void PrintContent(const Content& content)\n",
    "{\n",
    "    std::cout << content.GetValue() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    WithUniquePtr obj(\"My priceless text here!\");\n",
    "    \n",
    "    decltype(auto) content = obj.GetContent();\n",
    "    PrintContent(content);    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Releasing a `unique_ptr`\n",
    "\n",
    "To free manually the content of a `unique_ptr`:\n",
    "\n",
    "* Use `release()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    auto ptr = std::make_unique<int>(5);\n",
    "    ptr.release(); // Beware: `.` and not `->` as it is a method of the smart pointer class, not of the \n",
    "                   // underlying class!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Or assign `nullptr` to the pointer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "{\n",
    "    auto ptr = std::make_unique<int>(5);\n",
    "    ptr = nullptr;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `shared_ptr`\n",
    "\n",
    "The philosophy of `shared_ptr` is different: this kind of smart pointers is fully copyable, and each time a copy is issued an internal counter is incremented (and decremented each time a copy is destroyed). When this counter reaches 0, the underlying object is properly destroyed.\n",
    "\n",
    "As for `unique_ptr`, there is a specific syntax to build them (properly named `make_shared`...); it was introduced earlier (C++ 11) and is not just cosmetic: the compiler is then able to store the counter more cleverly if you use `make_shared` rather than `new` (so make it so!)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include <memory>\n",
    "\n",
    "{\n",
    "    std::shared_ptr<double> ptr = std::make_shared<double>(5.);\n",
    "    \n",
    "    auto ptr2 = ptr;\n",
    "    \n",
    "    std::cout << \"Nptr = \" << ptr.use_count() << std::endl; \n",
    "    //< Notice the `.`: we access a method from std::shared_ptr, not from the type encapsulated\n",
    "    // by the pointer!\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "`shared_ptr` are clearly useful, but you should always wonder first if you really need them: for most uses a `unique_ptr` eventually seconded by raw pointers extracted by `get()` is enough.\n",
    "\n",
    "There is also a risk of not releasing properly the memory is there is a circular dependency between two `shared_ptr`. A variation of this pointer named `weak_ptr` enables to circumvent this issue, but is a bit tedious to put into motion. I have written in [appendix](../7-Appendix/WeakPtr.ipynb) a notebook to describe how to do so.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Efficient storage with vectors of smart pointers\n",
    "\n",
    "* `std::vector` are cool, but the copy when capacity is exceeded might be very costly for some objects. Moreover, it forces you to provide copy behaviour to your classes intended to be stored in `std::vector`, which is not a good idea if you do not want them to be copied.\n",
    "\n",
    "* An idea could be to use pointers: copy is cheap, and there is no need to copy the underlying objects when the capacity is exceeded. Another good point is that a same object might be stored in two different containers, and the modifications given in one  of this is immediately \"seen\" by the other (as the underlying object is the same).\n",
    "However, when this `std::vector` of pointers is destroyed the objects inside aren't properly deleted, provoking memory leaks.\n",
    "\n",
    "\n",
    "The way to combine advantages without retaining the flaws is to use a vector of smart pointers:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <array>\n",
    "\n",
    "class NotCopyable\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        NotCopyable(double value);\n",
    "    \n",
    "        ~NotCopyable();\n",
    "\n",
    "        NotCopyable(const NotCopyable& ) = delete;    \n",
    "        NotCopyable& operator=(const NotCopyable& ) = delete;\n",
    "        NotCopyable(NotCopyable&& ) = delete;    \n",
    "        NotCopyable& operator=(NotCopyable&& ) = delete;\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        std::array<double, 1000> data_;\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NotCopyable::NotCopyable(double value)\n",
    "{\n",
    "    data_.fill(value);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "\n",
    "NotCopyable::~NotCopyable()\n",
    "{\n",
    "    std::cout << \"Call to NotCopyable destructor!\" << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <vector>\n",
    "#include <iostream>\n",
    "\n",
    "{\n",
    "    std::vector<std::unique_ptr<NotCopyable>> list;\n",
    "\n",
    "    for (double x = 0.; x < 8.; x += 1.1)\n",
    "    {\n",
    "        std::cout << \"Capacity = \" << list.capacity() << std::endl;\n",
    "        list.emplace_back(std::make_unique<NotCopyable>(x)); // emplace_back is like push_back for rvalues\n",
    "    }\n",
    "    \n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Doing so:\n",
    "\n",
    "- The `NotCopyable` are properly stored in a container.\n",
    "- No costly copy occurred: there were just few moves of `unique_ptr` when the capacity was exceeded.\n",
    "- The memory is properly freed when the `list` becomes out of scope.\n",
    "- And as we saw in previous section, the underlying data remains accessible through reference or raw pointer if needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using a trait as syntactic sugar\n",
    "\n",
    "I like to create aliases in my classes to provide more readable code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <array>\n",
    "#include <vector>\n",
    "\n",
    "class NotCopyable2\n",
    "{\n",
    "    public:\n",
    "    \n",
    "        // Trait to alias the vector of smart pointers.\n",
    "        using vector_unique_ptr = std::vector<std::unique_ptr<NotCopyable2>>;\n",
    "    \n",
    "        NotCopyable2(double value);\n",
    "\n",
    "        NotCopyable2(const NotCopyable2& ) = delete;    \n",
    "        NotCopyable2& operator=(const NotCopyable2& ) = delete;\n",
    "        NotCopyable2(NotCopyable2&& ) = delete;    \n",
    "        NotCopyable2& operator=(NotCopyable2&& ) = delete;\n",
    "    \n",
    "    private:\n",
    "    \n",
    "        std::array<double, 1000> data_; // not copying it too much would be nice!\n",
    "    \n",
    "};"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "NotCopyable2::NotCopyable2(double value)\n",
    "{\n",
    "    data_.fill(value);\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#include <iostream>\n",
    "#include<vector>\n",
    "\n",
    "{\n",
    "    // Use the alias\n",
    "    NotCopyable2::vector_unique_ptr list;\n",
    "    \n",
    "    // or not: it amounts to the same!\n",
    "    std::vector<std::unique_ptr<NotCopyable2>> list2;\n",
    "    \n",
    "    // std::boolalpha is just a stream manipulator to write 'true' or 'false' for a boolean\n",
    "    std::cout << std::boolalpha << std::is_same<NotCopyable2::vector_unique_ptr, std::vector<std::unique_ptr<NotCopyable2>>>() << std::endl;\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This simplifies the reading, especially if templates are also involved... "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# References\n",
    "\n",
    "[<a id=\"cit-Alexandrescu2001\" href=\"#call-Alexandrescu2001\">Alexandrescu2001</a>] Andrei Alexandrescu, ``_Modern C++ Design: Generic Programming and Design Patterns applied_'', 01 2001.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "[© Copyright](../COPYRIGHT.md)   \n",
    "",
    ""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "C++17",
   "language": "C++17",
   "name": "xcpp17"
  },
  "language_info": {
   "codemirror_mode": "text/x-c++src",
   "file_extension": ".cpp",
   "mimetype": "text/x-c++src",
   "name": "c++",
   "version": "17"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "key",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of contents",
   "title_sidebar": "Contents",
   "toc_cell": true,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
