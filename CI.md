Currently CI is used to generate Docker images related to the training.

These images are created through the Gitlab Web interface:

- Go to CI/CD and choose Run Pipeline.
- Fill a field _TAG_ and put the name of the tag to associate to the image.
- Fill a field _UPDATE_LATEST_TAG_ if you want the _latest_ tag to be updated with the image being created here. This field is optional - if not filled the _latest_ tag won't be modified.

