#include <iostream>
#include <cmath> // for std::round


//! Returns `number` * (2 ^ `exponent`) 
int TimesPowerOf2(int number, int exponent)
{
    while (exponent > 0)
    { 
        number *= 2; 
        exponent -= 1; 
    }
    while (exponent < 0)
    { 
        number /= 2;
        exponent += 1 ; 
    }
    
    return number;
}
    

//! Round to `x` the nearest integer.
int RoundAsInt(double x)
{
    return static_cast<int>(std::round(x));
}


//! Display the approximation of the given argument for the exponents from 1 to 8.
void DisplayPowerOf2Approx(double value)
{
    for (int exponent = 1; exponent <= 8; ++exponent)
    {
        int denominator = TimesPowerOf2(1, exponent);        
        int numerator = RoundAsInt(value * denominator);
        double approx = static_cast<double>(numerator) / denominator; 
        
        std::cout << value << " ~ " << approx << " (" << numerator << 
            " / 2^" << exponent << ')' << std::endl;
    }
    
    std::cout << std::endl;
    
}



/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother 

    DisplayPowerOf2Approx(.65);
    DisplayPowerOf2Approx(.35);
    
    return EXIT_SUCCESS;
}

