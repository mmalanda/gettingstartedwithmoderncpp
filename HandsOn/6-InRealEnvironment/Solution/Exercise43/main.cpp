#include <vector>
#include <memory>

#include "Tools.hpp"
#include "PowerOfTwoApprox.hpp"
#include "TestDisplay.hpp"




//! For each container stored, loop oover all those bits and print the result on screen.
void Loop(int initial_Nbit, int final_Nbit, int increment_Nbit, const std::vector<std::unique_ptr<TestDisplay>>& container)
{
    for (const auto& ptr : container)        
    {
        assert(ptr != nullptr);
        const auto& current_test_display = *ptr;
                    
        for (int nbits = initial_Nbit; nbits <= final_Nbit; nbits += increment_Nbit)
            current_test_display(nbits);

        std::cout << std::endl;
    }
}


/************************************/
// Main function
/************************************/

int main(int argc, char** argv)
{
    static_cast<void>(argc); // to silence warning about unused argc - don't bother 
    static_cast<void>(argv); // to silence warning about unused argv - don't bother      

    try
    {
        std::vector<std::unique_ptr<TestDisplay>> container;
    
        using integer_type = long;
    
        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApproxMinus065<integer_type>>(100000000));
        container.emplace_back(std::make_unique<TestDisplayPowerOfTwoApprox035<integer_type>>(100000000));
        container.emplace_back(std::make_unique<TestDisplaySumOfMultiply<integer_type>>(1000000));
    
        Loop(4, 32, 4, container);
    }
    catch(const std::exception& e)
    {
        std::cerr << "An error occurred in the program: " << e.what() << std::endl;
    }
    
    return EXIT_SUCCESS;
}

