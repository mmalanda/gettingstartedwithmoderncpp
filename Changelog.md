We are using here tghe same convention as Ubuntu: a release is typically named vYY.MM, where YY is the last two digits of current year and MM two digits representing the month.

# rc24.03

There were 3 lecturers for this session:

- Jérôme Diaz [^m3disim]
- Sébastien Gilles [^sed-saclay]
- Vincent Rouvreau [^sed-saclay]

- #102 Add a sentence to underline C++ functor is not FP functor.
- #98 In Streams, manipulators section, `#include <iomanip>` is missing for `setprecision`.
- #96 PrintDivision does not show what the example should.
- #93 Update Openclassroom link.
- #92 Add prints to illustrate accuracy loss.
- #89 Doxygen: remove the sentences related to its slowness that are no longer true.
- #88 Tools: add a reference to codespell.
- #87 Remove redundant sentence.
- #86 Replace concurrent by competitor.
- #84 Add link to page with support of new features by compilers.
- #82 Add mention of the parallelism policy for STL algorithms.
- #78 Add the contiguity in memory for `std::array` which was a glaring overlook, especially with the line related to `std::string` that told it was the sole container besides `std::vector` to guarantee it.
- #75 Add Coliru link.
- #74 Missing parenthesis.
- #91 Metaprogramming notebook:    
    * Add a refinement to the example that uses up `if constexpr`.
    * Add an example of `std::apply`
- #73 Explain explicitly why a `for` loop doesn't work for tuples, and add reminder of the trick to work around the impossibility to specialize template functions.
- #72 Put more emphasis on the fact it is an overload and not a template partial specialization that takes place in the example.
- #63 Add an example of write access through friendship.
- #61 Lift a possible ambiguity between class name `Array` and its data `attribute array_` by renaming the latter.
- #108 Fix a dead link. Many thanks to Mathias Malandain for finding it out and providing a link to the web archives!
- #60 Detail a bit more the data attribute initialization through the `:` syntax.
- #97 Add a paragraph to explain in C functions overload are not possible.
- #43 Update Copyright file.
- #29 Remove an ambiguous sentence.



# [v22.10](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/releases/v22.10)

This version was not emitted properly right after the training session and has been created just now in February 2024.

Exact changes will be added later (see [issue #110](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/issues/110)).

There were 3 lecturers for this session:

- Laurent Steff [^sed-saclay]
- Sébastien Gilles [^sed-saclay]
- Vincent Rouvreau [^sed-saclay]


# [v21.05.2](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/releases/v21.05.2)

Version after various fixes given during the training session.

## Notebooks

- #20 Rename the _TP_ notebooks _HandsOn_.


# [v21.05](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/releases/v21.05)

A third training session was given in [May 2021](https://sed.saclay.inria.fr/formations/2021-05-17-cpp/), under a slightly different format due to Covid 19 crisis: 5 half days in visio and no hands-ons during the sessions.

There are 4 lecturers for this session:

- Laurent Steff [^sed-saclay]
- Sébastien Gilles [^sed-saclay]
- Vicente Mataix-Ferrandiz [^sed-paris]
- Vincent Rouvreau [^sed-saclay]

There were more substantial changes in the lecture; the hands-ons were however left untouched.

## Docker and Jupyter

- Notebooks may be run through BinderHub.
- Addition of Docker images to run the notebooks.
- Streamlining of existing Docker images.
- All Docker images may now be created through the Gitlab Web interface with CI/CD.
- Jupyter-lab is now used instead of vanilla Jupyter.

## Notebooks

- The notebook related to inheritance has been heavily refactored and ultimately split in two notebooks: one for inheritance and the other for polymorphism.
- A new notebook to present `constexpr` has been added; content from another notebook related to `static` for local variables has been moved there as well.
- New notebook in appendix about `std::string_view`.
- All notebooks have been reread and amended, with few content added here and there (including few sparse remarks about C++20).

## Miscellaneous

- A _CONTRIBUTING.md_ file was added explaining the workflow for contribution; a commit hook was also created to avoid committing a notebook with cells


# [v20.03](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/releases/v20.03)

A second training session was given in [March 2020](https://sed.saclay.inria.fr/formations/2021-05-17-cpp/) but was ultimately interrupted in its middle by the Covid 19 shutdown. The lecturers were the same as for v19.05 below.

The (relatively minor) modifications were:

- Cleaning-up of all notebooks.
- Reorganization of the hands-ons, which was deemed too bulky in the first training session.



# [v19.05](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/releases/v19.05)

Current lecture was given for the first time in [May 2019](https://sed.saclay.inria.fr/formations/2019-05-cpp/) by Sébastien Gilles [^sed-saclay] and Vincent Rouvreau [^sed-saclay].


# [Original lecture](https://gitlab.inria.fr/FormationCpp/DebuterEnCpp)

This tutorial is heavily inspired from a C++ tutorial created by David Chamont (CNRS) that was given as a lecture with the help of Vincent Rouvreau (Inria SED [^sed-saclay] Saclay) in 2016.

This original version was in French and didn't use Jupyter notebooks; the bulk of the lecture is in the project [wiki](https://gitlab.inria.fr/formations/cpp/DebuterEnCpp/-/wikis/home).




[^sed-saclay]: [Center Inria Saclay-Ile-de France](https://www.inria.fr/fr/centre-inria-saclay-ile-de-france) - [SED](https://sed.saclay.inria.fr/)[^sed]
[^sed-paris]: [Center Inria Paris](https://www.inria.fr/fr/centre-inria-de-paris) - [SED](https://sed.paris.inria.fr/)[^sed]
[^m3disim]: [Center Inria Saclay-Ile-de France](https://www.inria.fr/fr/centre-inria-saclay-ile-de-france) - [M3DISIM team](https://m3disim.saclay.inria.fr)
[^sed]: SED is an acronym meaning _Experimentation and Development Department_; there is one such department per [Inria](https://www.inria.fr) center. 